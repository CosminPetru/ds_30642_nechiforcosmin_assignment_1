import dal.CityDAL;
import dal.FlightDAL;
import dal.UserDAL;
import entity.City;
import entity.Flight;
import entity.User;
import service.ClientService;

import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;

public class TestDAO {
    public static void main(String[] args) throws ProtocolException {
//        TestDAO.insertFlight();
    }

    private static void checkUser() {
        User user = new User("asd", "asd", "asd", "asd@asd.com");
        UserDAL userDAL = new UserDAL();

        userDAL.save(user);

        List<User> userList = userDAL.findAll();
        userList.forEach(u -> System.out.println(u));

    }

    private static void checkFlightCity() {
        City start = new City("asd", "add", "asd");
        City end = new City("asd2", "add2", "asd2");

        Flight flight = new Flight("asd", "pseudo", start, end, "10101010", "123412");

        System.out.println(flight.getArrivalCity().getName());
        System.out.println(flight.getDepartureCity().getName());

        FlightDAL flightDAL = new FlightDAL();
        flightDAL.save(flight);
        System.out.println("Flight inserted");

        for (Flight f : flightDAL.findAll()) {
            System.out.println(f.getDepartureCity());
        }
    }

    private static void populate() {
        List<City> cityList = new ArrayList<>();
        cityList.add(new City("22", "22", "Bucuresti"));
        cityList.add(new City("22", "22", "Cluj"));
        cityList.add(new City("22", "22", "Berlin"));
        cityList.add(new City("22", "22", "Madrid"));
        cityList.add(new City("22", "22", "Barcelona"));
        cityList.add(new City("22", "22", "London"));
        cityList.add(new City("22", "22", "Meinz"));
        CityDAL cityDAL = new CityDAL();
        for (City c :
                cityList) {
            cityDAL.save(c);
        }
    }

    private static void insertFlight() {
        FlightDAL flightDAL = new FlightDAL();
        CityDAL cityDAL = new CityDAL();
        City berlin = cityDAL.findByName("Berlin");
        System.out.println(berlin);
        City cluj = cityDAL.findByName("Cluj");
        System.out.println(cluj);


        Flight flight = new Flight();
        flight.setArrivalTime("12333");
        flight.setDepartureTime("1233");
        flight.setDepartureCity(berlin);
        flight.setArrivalCity(cluj);
        flight.setPseudo("asd");
        flight.setAirplaneType("pass");

        System.out.println("Insert flight");
        flightDAL.save(flight);
        System.out.println("Inserted");
    }
}
