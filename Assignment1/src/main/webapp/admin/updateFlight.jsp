<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: cosmin
  Date: 14.10.2018
  Time: 18:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update Flight</title>
</head>
<body>
<form method="get" action="/deleteFlight">
    <input type="submit" value="Delete Flight">
</form>
<form method="get" action="/addFlight">
    <input type="submit" value="Add Flight">
</form>
<form method="post" action="/logout">
    <input type="submit" value="logout button">
</form>
<h1>Admin update page</h1>
<fieldset>
    <legend>Flights</legend>
    <table style="width:100%">
        <tr>
            <th>id</th>
            <th>Airplane type</th>
            <th>pseudo</th>
            <th>Arrival city</th>
            <th>Arrival time</th>
            <th>Departure city</th>
            <th>Departure time</th>
        </tr>
        <c:forEach items="${flights}" var="flight" varStatus="status">
            <tr>
                <td>${flight.id}</td>
                <td>${flight.airplaneType}</td>
                <td>${flight.pseudo}</td>
                <td>${flight.arrivalCity}</td>
                <td>${flight.departureCity}</td>
                <td>${flight.arrivalTime}</td>
                <td>${flight.departureTime}</td>
            </tr>
        </c:forEach>
    </table>
</fieldset>
<%--Delete--%>
<form method="post" action="/updateFlight">
    <fieldset>
        <legend>Delete flight</legend>
        <p>
            <label>id: <br>
                <input type="text" name="update_id" required/>
            </label>
        </p>
        <p>
            <label>Type Avion: <br>
                <input type="text" name="typeAirplane" required/>
            </label>
        </p>
        <p>
            <label>Pseudo: <br>
                <input type="text" name="pseudo" required/>
            </label>
        </p>
        <p>
            <label>Starting city:
                <input type="text" name="start_city" required/>
                <input type="time" name="take_off_time" required/>
                <input type="date" name="take_off_date" required/>
            </label>
        </p>
        <p>
            <label>Destination:
                <input type="text" name="end_city" required/>
                <input type="time" name="landing_time" required/>
                <input type="date" name="landing_date" required/>
            </label>
        </p>
        <p>
            <input type="submit" value="Update">
        </p>
    </fieldset>
</form>
</body>
</html>
</body>
</html>