<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: cosmin
  Date: 14.10.2018
  Time: 20:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form method="post" action="/logout">
    <input type="submit" value="logout button">
</form>

<h1>Client page</h1>
<fieldset>
    <legend>Flights</legend>
    <table style="width:100%">
        <tr>
            <th>id</th>
            <th>Airplane type</th>
            <th>pseudo</th>
            <th>Departure city</th>
            <th>Departure time</th>
            <th>Arrival city</th>
            <th>Arrival time</th>
        </tr>
        <c:forEach items="${flights}" var="flight" varStatus="status">
            <tr>
                <td>${flight.id}</td>
                <td>${flight.airplaneType}</td>
                <td>${flight.pseudo}</td>
                <td>${flight.departureCity}</td>
                <td>${flight.departureTime}</td>
                <td>${flight.arrivalCity}</td>
                <td>${flight.arrivalTime}</td>
            </tr>
        </c:forEach>
    </table>
</fieldset>
<form method="post" action="/client">
    <fieldset>
        <legend>Date time for coordinates</legend>
        <label>lat:
            <input type="text" name="lat" required/>
        </label>
        <label>long:
            <input type="text" name="lng" required/>
        </label>
        <br>
        <label>
            Datetime: ${datetime}
        </label>
        <br>
       <p>
        <input type="submit" value="Update">
        </p>
    </fieldset>
</form>
</body>
</html>
