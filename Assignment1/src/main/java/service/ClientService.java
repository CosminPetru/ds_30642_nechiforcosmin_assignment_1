package service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

public class ClientService {

    private final String USER_AGENT = "Mozilla/5.0";

    public String getDateTimeInfo(double lat, double lng) throws ProtocolException {
        String url = "http://api.timezonedb.com/v2.1/get-time-zone?key=Y0K06AQI4M8R&format=json&by=position&lat="+ lat +"&lng=" + lng;
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = 0;
            responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            String responseString = response.toString();

            return (String) responseString.subSequence(responseString.length() - 21, responseString.length() - 2);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
