package service;

import dal.CityDAL;
import entity.City;

import java.util.List;

public class CityService {
    private CityDAL cityDAL = new CityDAL();

    public List<City> findAll() {
       return cityDAL.findAll();
    }

    public City findByName(String name) {
        return cityDAL.findByName(name);
    }
}
