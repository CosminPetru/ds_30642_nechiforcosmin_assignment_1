package service;

import dal.FlightDAL;
import dal.GenericDAL;
import entity.Flight;

import java.util.List;

public class FlightService {
    private FlightDAL flightDAL = new FlightDAL();

    public List<Flight> findAll() {
        return flightDAL.findAll();
    }

    public void add(Flight flight) {
        System.out.println("Adding flight");
        flightDAL.save(flight);
    }

    public void deleteById(int id) {
        flightDAL.deleteById(id);
    }

    public Flight findById(int parseInt) {
        return flightDAL.findOne(parseInt);
    }

    public void update(Flight flight) {
        flightDAL.update(flight);
    }
}
