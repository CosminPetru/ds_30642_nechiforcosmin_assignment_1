package service;

import dal.UserDAL;
import entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserService {

    private UserDAL userDAL = new UserDAL();

    public User login(String username, String password) {
        return userDAL.checkCredentials(username, password);
    }

    public static int filter(HttpServletRequest request, String param) {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") == null) {
            return -1;
        } else {
            User user = (User) session.getAttribute("user");
            if (user.getType().equals(param))
                return 0;
            return 1;
        }
    }
}
