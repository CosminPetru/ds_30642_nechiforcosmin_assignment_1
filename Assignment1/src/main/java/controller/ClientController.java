package controller;

import entity.Flight;
import service.ClientService;
import service.FlightService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/client")
public class ClientController extends HttpServlet {

    private FlightService flightService = new FlightService();
    private ClientService clientService = new ClientService();
    private String dateTime = null;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int condition = UserService.filter(request, Constants.CLIENT);
        if (condition == 0) {
            List<Flight> citiesList = flightService.findAll();
            request.setAttribute("flights", citiesList);
            request.setAttribute("datetime", dateTime);
            request.getRequestDispatcher("client/client.jsp").forward(request, response);
        } else if (condition == 1){
            response.sendRedirect("/addFlight");
        } else {
            request.setAttribute("error_message", "You are not allowed!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doPost from client controller was called");
        String lng = request.getParameter("lng");
        String lat = request.getParameter("lat");
        double dlng = Double.parseDouble(lng);
        double dlat = Double.parseDouble(lat);
        System.out.println("dlng: " + dlng + " dlat: " + dlat);
        String newDataTime = clientService.getDateTimeInfo(dlat, dlng);
        this.dateTime = newDataTime;
        response.sendRedirect("/client");
    }
}
