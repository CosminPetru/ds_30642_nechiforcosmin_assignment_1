package controller;

import entity.City;
import entity.Flight;
import service.CityService;
import service.FlightService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateFlight")
public class UpdateFlightController extends HttpServlet {
    private FlightService flightService = new FlightService();
    private CityService cityService = new CityService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("update flight get is being called");
        int condition = UserService.filter(request, Constants.ADMIN);
        if (condition == 0) {
            request.setAttribute("flights", flightService.findAll());
            request.setAttribute("cityList", cityService.findAll());
            request.getRequestDispatcher("admin/updateFlight.jsp").forward(request, response);
        } else if (condition == 1){
            request.setAttribute("error_message", "Permision denied!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } else {
            response.sendRedirect("/");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("update flight post called");

        String id = request.getParameter("update_id");
        Flight flight = flightService.findById(Integer.parseInt(id));

        String airplaneType = request.getParameter("typeAirplane");
        String pseudo = request.getParameter("pseudo");
        String start_city = request.getParameter("start_city");
        String end_city = request.getParameter("end_city");
        String arrivalDate = request.getParameter("landing_date");
        String arrivalTime = request.getParameter("landing_time");
        String takeOffTime = request.getParameter("take_off_time");
        String takeOffDate = request.getParameter("take_off_date");

        City departureCity = cityService.findByName(start_city);
        City arrivalCity = cityService.findByName(end_city);

        flight.setAirplaneType(airplaneType);
        flight.setPseudo(pseudo);
        flight.setArrivalCity(arrivalCity);
        flight.setDepartureCity(departureCity);
        flight.setDepartureTime(takeOffDate + " " + takeOffTime);
        flight.setArrivalTime(arrivalDate + " " + arrivalTime);

        flightService.update(flight);

        response.sendRedirect("/updateFlight");
    }
}
