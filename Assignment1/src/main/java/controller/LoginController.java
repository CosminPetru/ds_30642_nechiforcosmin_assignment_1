package controller;

import entity.Flight;
import entity.User;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/login")
public class LoginController extends HttpServlet {
    private UserService userService = new UserService();
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user != null ) {
            int condition = UserService.filter(request, Constants.CLIENT);
            if (condition == 0) {
                response.sendRedirect("/client");
                return;
            } else {
                response.sendRedirect("/deleteFlight");
                return;
            }
        }
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String userName = request.getParameter("username");
        String password = request.getParameter("password");

        User user = null;
        try{
            user = userService.login(userName, password);
        }catch(Exception e) {
            request.setAttribute("error_message", "Wrong credentials!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
        if (user == null){
            request.setAttribute("error_message", "Wrong account!");
            response.sendRedirect("/");
        }
        else {
            request.getSession().setAttribute("user", user);
            if (user.getType().equals(Constants.ADMIN)) {
                response.sendRedirect("/addFlight");
            }
            else {
                response.sendRedirect("/client");
            }
        }
    }
}

