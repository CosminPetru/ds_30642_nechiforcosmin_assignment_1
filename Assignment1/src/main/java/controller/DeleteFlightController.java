package controller;

import service.FlightService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/deleteFlight")
public class DeleteFlightController extends HttpServlet {
    private FlightService flightService = new FlightService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Add flight is being called");
        int condition = UserService.filter(request, Constants.ADMIN);
        if (condition == 0) {
            request.setAttribute("flights", flightService.findAll());
            request.getRequestDispatcher("admin/deleteFlight.jsp").forward(request, response);
        } else if (condition == 1){
            request.setAttribute("error_message", "Permision denied!");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } else {
            response.sendRedirect("/");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("delete flight post called");
        String airplaneType = request.getParameter("delete_id");
        flightService.deleteById(Integer.parseInt(airplaneType));
        response.sendRedirect("/deleteFlight");
    }
}
