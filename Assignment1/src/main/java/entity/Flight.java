package entity;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;

@Entity
@OptimisticLocking(type = OptimisticLockType.ALL)
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "flight")
public class Flight {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "airplane_type")
    private String airplaneType;

    @Column(name = "pseudo")
    private String pseudo;

    @OneToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="arrival_city", referencedColumnName="id")
    private City arrivalCity;

    @OneToOne(fetch=FetchType.EAGER, optional=false)
    @JoinColumn(name="departure_city", referencedColumnName="id")
    private City departureCity;

    @Column(name = "departure_time")
    private String departureTime;

    @Column(name = "arrival_time")
    private String arrivalTime;

    public Flight(){}

    public Flight(String airplaneType, String pseudo, City arrivalCity, City departureCity, String departureTime, String arrivalTime) {
        this.airplaneType = airplaneType;
        this.pseudo = pseudo;
        this.arrivalCity = arrivalCity;
        this.departureCity = departureCity;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
    }

    public int getId() {
        return id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplane_type) {
        this.airplaneType = airplane_type;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrival_city) {
        this.arrivalCity = arrival_city;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departure_city) {
        this.departureCity = departure_city;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrival_time) {
        this.arrivalTime = arrival_time;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departure_time) {
        this.departureTime = departure_time;
    }
}
