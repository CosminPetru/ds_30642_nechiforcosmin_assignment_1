package dal;

import entity.City;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

public class CityDAL extends GenericDAL<City> {

    public CityDAL(){
        super(City.class);
    }

    public City findByName(String name) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<City> query = cb.createQuery(City.class);
        ParameterExpression<String> city = cb.parameter(String.class, "name");
        Root<City> cityRoot = query.from(City.class);
        query.select(cityRoot).orderBy(cb.asc(cityRoot.get("name")));
        query.where(cb.equal(cityRoot.get("name"), city));
        TypedQuery<City> typedQuery = entityManager.createQuery(query);
        return typedQuery.setParameter("name", name).getSingleResult();
    }
}
