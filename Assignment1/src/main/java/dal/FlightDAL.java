package dal;

import entity.Flight;

public class FlightDAL extends GenericDAL<Flight>{

    public FlightDAL(){
        super(Flight.class);
    }
}
