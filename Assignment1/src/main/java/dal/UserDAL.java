package dal;

import entity.User;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

public class UserDAL extends GenericDAL<User> {

    public UserDAL(){
        super(User.class);
    }

    public User checkCredentials(String name, String password) {
       CriteriaBuilder cb = entityManager.getCriteriaBuilder();
       CriteriaQuery<User> query = cb.createQuery(User.class);
       ParameterExpression<String> user = cb.parameter(String.class, "username");
       Root<User> userRoot = query.from(User.class);
       query.select(userRoot).orderBy(cb.asc(userRoot.get("username")));
       query.where(cb.equal(userRoot.get("username"), user));
       TypedQuery<User> typedQuery = entityManager.createQuery(query);
       User toReturn = typedQuery.setParameter("username", name).getSingleResult();
       if (toReturn.getPassword().equals(password)) {
           return toReturn;
       }
       return null;
    }

}
