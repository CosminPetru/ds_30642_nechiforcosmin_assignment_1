# Assignment 1.2

This is the description of the second project at Distributed Systems. 

This project was built using WebServlets. The documentation was not added, because I didn't made it 
and I accept losing the points for that. 

## Technologies

In the development of this project i used the following technologies:

- HTML 
- Java Servlets 
- Hibernate ORM 

## How to start it:

The code can be easily run in Intellij after Maven will finish with
getting all the dependencies .

Or if Intellij doesn't get you dependencies by default, you can run the following command: 
``mvn install `` or ``mvn package`` which will always work.


## Diagrams:

The diagrams were made in an open source tool.

![Class Diagram](Diagrama_DS.png)


The database diagram is the same as the class diagram for the models so I won't
add one that it's only for the database.

![Model Diagram](model_diagram.png)




## Code for generating the diagrams on your own (if you would want that):

```$xslt

# Assignment 1 - Airport


## Class Diagram


``uml

[GenericDAL
<<abstract>>
|
- clazz: T
|
+ findOne(id: int): T
+ findAll(): List<T>
+ save(): void
+ update(): void
+ delete(): void
+ deleteById(id: int): void
] <:-[UserDAL]

[UserDAL
|

|
+ checkCredentials(name: String, password: String): User
]

[GenericDAL] <:- [FlightDAL||]

[GenericDAL] <:- [CityDAL]

[CityDAL
|
|
+ findByName(name: String): City
]




[CityService|
- ciryDAL: CityDAL
|
+ findByName(name: String): City
]

[UserService|
- userDAL: UserDAL
|
+ login(username: String, password: String)
]


[FlightService|
- flightService: FlightService
|
+ findAll(): List<Flight>
+ add(flight: Flight): void
+ deleteById(id: int): void
+ findById(parser: int): Flight
+ update(flight: Flight): void
]


[FlightService] -+ [FlightDAL]
[CityService] -+ [CityDAL]
[UserService] -+ [UserDAL]



[ClinetService|
- USER_AGENT: String

|

+ getDateTimeInfo(lat: double, len: double): String
]


[HttpServlet]


[AddFlightController

|
- flightService: FlightService
- cityService: CityService
|
`# doGet(request: HttpServeltRequest, response: HttpServletResponse): void
`# doPost(request: HttpServeltRequest, response: HttpServletResponse): void
]


[ClientController
|
- flightService: FlightService
- clinetService: ClientService
|
`# doGet(request: HttpServeltRequest, response: HttpServletResponse): void
`# doPost(request: HttpServeltRequest, response: HttpServletResponse): void
]



[DeleteFlightController

|
- flightService: FlightService
|
`# doGet(request: HttpServeltRequest, response: HttpServletResponse): void
`# doPost(request: HttpServeltRequest, response: HttpServletResponse): void
]



[LoginController
|
- userService: UserService
|
`# doGet(request: HttpServeltRequest, response: HttpServletResponse): void
`# doPost(request: HttpServeltRequest, response: HttpServletResponse): void
]



[LogoutController

|
|
`# doGet(request: HttpServeltRequest, response: HttpServletResponse): void
`# doPost(request: HttpServeltRequest, response: HttpServletResponse): void
]


[UpdateFlightController

|
- flightService: FlightService
- cityService: CityService
|
`# doGet(request: HttpServeltRequest, response: HttpServletResponse): void
`# doPost(request: HttpServeltRequest, response: HttpServletResponse): void
]


[HttpServlet] <:-[LoginController]

[HttpServlet] <:-[AddFlightController]
[HttpServlet] <:- [ClientController]
[HttpServlet] <:- [DeleteFlightController]
[HttpServlet] <:- [LogoutController]
[HttpServlet] <:- [UpdateFlightController]


[AddFlightController] -+ [FlightService]
[AddFlightController] -+ [CityService]


[DeleteFlightController] -+ [FlightService]


[UpdateFlightController] -+ [FlightService]
[UpdateFlightController] -+ [CityService]


[ClientController] -+ [FlightService]
[ClientController] -+ [CityService]



[LoginController] -+ [UserService]

[Constants|

+ ADMIN: String
+ CLIENT: String
]


[Constants] <- [LoginController]
[Constants] <- [AddFlightController]
[Constants] <- [DeleteFlightController]
[Constants] <- [UpdateFlightController]
[Constants] <- [ClientController]
[Constants] <- [LoginController]


``

## Entities


``uml

[User
|
- id: Integer
- username: String
- password: String
- type: String
- email: String
|

+ get/set
]


[City
|
- id: Integer
- longitude: String
- latitude: String
- name: String
|
+ get/set
+ toString(): String
]

[Flight
|
- id: Integer
- airplaneType: String
- pseudo: String
- arrivalCity: City
- arrivalTime: String
- departureCity: City
- departureTime: String
|
+ get/set
]

[City] -+ [Flight]

```


Author: Cosmin Nechifor


